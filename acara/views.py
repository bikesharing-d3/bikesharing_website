from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib import messages
from django.urls import reverse
from django.db import connection
from django.shortcuts import render

# Create your views here.
def create(request):
    response = {}
    cursor = connection.cursor()
    cursor.execute('set search_path to bikesharing,public')
    cursor.execute("select nama from stasiun")

    hasil2 = cursor.fetchall()
    response['stasiun'] = hasil2
    return render(request, 'Create.html', response)


def daftar(request):
    response = {}
    cursor = connection.cursor()
    cursor.execute('set search_path to bikesharing,public')
    cursor.execute("select * from acara")

    hasil2 = cursor.fetchall()
    response['acara'] = hasil2
    return render(request, 'Dafta_acara.html', response)

def postAcara(request):
    if (request.method == "POST"):
        judul = request.POST['judul']
        deskripsi = request.POST['deskripsi']
        fee = request.POST['fee']
        mulai = request.POST['mulai']
        selesai = request.POST['selesai']
        list_stasiun = request.POST.getlist('stasiun')
        biaya = False
        if(fee == 'gratis'):
            biaya = True
        else:
            biaya = False

        cursor = connection.cursor()
        cursor.execute('set search_path to bikesharing,public')
        cursor.execute(
            "select id_acara from acara order by cast (id_acara as int) desc;")
        hasil = cursor.fetchone()
        id_acara = int(hasil[0]) + 1
        cursor.execute("INSERT INTO acara(id_acara,judul,tgl_mulai,is_free,tgl_akhir,deskripsi) values("+"'"+str(id_acara) +
                       "','" + str(judul)+"','" + mulai + "'," + str(biaya) + ",'" + selesai + "','" + str(deskripsi) + "');")

        for i in list_stasiun:
            cursor.execute(
                "select id_stasiun from stasiun  where nama ='"+i+"' ")
            id_stasiun = cursor.fetchone()
            cursor.execute("INSERT INTO acara_stasiun(id_stasiun,id_acara) values(" +
                           "'"+str(id_stasiun[0])+"','" + str(id_acara) + "');")

        messages.error(request, "success")
        return HttpResponseRedirect('/acara/listacara')
    else:
        messages.error(request, "gagal")
        return HttpResponseRedirect('/acara/createacara')


@csrf_exempt
def deleteAcara(request):
    if(request.method == "POST"):

        id_acara = request.POST['id']
        cursor = connection.cursor()
        cursor.execute('set search_path to bikesharing,public')
        cursor.execute("delete from acara where id_acara='"+id_acara+"';")
        messages.error(request, "success")
        return HttpResponseRedirect('/acara/listacara')

    else:
        messages.error(request, "gagal")
        return HttpResponseRedirect('/acara/listacara')


@csrf_exempt
def updateAcara(request):
    if(request.method == "POST"):
        id_acara = request.POST['id_acara']
        judul = request.POST['judul']
        deskripsi = request.POST['deskripsi']
        fee = request.POST['fee']
        mulai = request.POST['mulai']
        selesai = request.POST['selesai']
        list_stasiun = request.POST.getlist('stasiun[]')
        biaya = False
        if(fee == 'gratis'):
            biaya = True
        else:
            biaya = False

        cursor = connection.cursor()
        cursor.execute('set search_path to bikesharing,public')
        cursor.execute("update acara set judul='"+judul+"', is_free='"+str(biaya)+"', tgl_mulai='" +
                       mulai+"', tgl_akhir='"+selesai+"', deskripsi='"+deskripsi+"' where id_acara = '"+id_acara+"';")
        cursor.execute("select s.nama from stasiun as s, acara_stasiun as a where a.id_acara ='" +
                       id_acara+"' and a.id_stasiun = s.id_stasiun;")
        nama_stasiun = cursor.fetchall()
        for i in range(0, len(list_stasiun)):

            id_stasiun_lama = ''
            id_stasiun = ''
            for i in nama_stasiun:
                print(i[0])
                cursor.execute(
                    "select id_stasiun from stasiun  where nama ='"+i[0]+"' ")
                id_stasiun_lama = cursor.fetchone()
                id_stasiun_lama = id_stasiun_lama[0]

            for j in list_stasiun:
                cursor.execute(
                    "select id_stasiun from stasiun  where nama ='"+j+"' ")
                id_stasiun = cursor.fetchone()
                id_stasiun = id_stasiun[0]

            cursor.execute("update acara_stasiun set id_stasiun='"+id_stasiun +
                           "' where id_acara='"+id_acara+"' and id_stasiun='"+id_stasiun_lama+"';")

        messages.error(request, "success")
        return HttpResponseRedirect('/acara/listacara')
    else:
        messages.error(request, "gagal")
        return HttpResponseRedirect('/acara/listacara')


@csrf_exempt
def detailUpdateAcara(request):
    if(request.method == "POST"):
        response = {}

        acara = request.POST['id']
        cursor = connection.cursor()
        cursor.execute('set search_path to bikesharing,public')
        cursor.execute("select * from acara where id_acara='"+acara+"';")
        id_acara = cursor.fetchone()
        cursor.execute("select nama from stasiun")
        stasiun = cursor.fetchall()
        cursor.execute("select s.nama from stasiun as s, acara_stasiun as a where a.id_acara ='" +
                       acara+"' and a.id_stasiun = s.id_stasiun;")
        nama_stasiun = cursor.fetchall()
        response['stasiun'] = stasiun
        response['acara'] = id_acara
        response['nama_stasiun'] = nama_stasiun
        messages.error(request, "success")
        return JsonResponse(response)

    else:
        messages.error(request, "gagal")
        return HttpResponseRedirect('/acara/listacara')