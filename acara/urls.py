from django.urls import path
from . import views
from django.contrib import admin

#url for app
urlpatterns = [
    path('listacara/', views.daftar, name='listsacara'),
    path('createacara/', views.create, name='createacara'),
    path('post-acara/', views.postAcara, name='postAcara'),
    path('delete-acara/', views.deleteAcara, name='deleteAcara'),
    path('update-acara/', views.updateAcara, name='updateAcara'),
    path('detail-update-acara/', views.detailUpdateAcara, name='detailUpdateAcara'),
]