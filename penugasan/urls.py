from django.urls import path
from penugasan import views
from django.contrib import admin

#url for app
urlpatterns = [
    path('', views.lists, name='listpetugas'),
    path('create/', views.create, name='createpetugas'),
    path('update-penugasan/', views.update_penugasan, name='updatePenugasan'),
    path('detail-penugasan/', views.detailPenugasan, name='detailPenugasan'),
    path('delete-penugasan/', views.delete_penugasan, name='deletePenugasan'),
    path('post-penugasan/', views.post_penugasan, name='postPenugasan'),
    path('delete-penugasan/', views.delete_penugasan, name='deletePenugasan'),
]
