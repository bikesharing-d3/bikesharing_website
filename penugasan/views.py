from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib import messages
from django.urls import reverse
from django.db import connection
from django.shortcuts import render

# Create your views here.
response = {}

def lists(request):
    response = {}
    cursor = connection.cursor()
    cursor.execute('set search_path to bikesharing,public')
    cursor.execute(
        "select t.ktp, r.nama, n.start_datetime, n.end_datetime, s.nama from petugas as t, person as r, penugasan as n, stasiun as s where r.ktp = t.ktp AND t.ktp = n.ktp AND n.id_stasiun = s.id_stasiun;")

    lst = cursor.fetchall()
    response['petugas'] = lst
    return render(request, 'lists.html', response)

def create(request):
    response = {}
    cursor = connection.cursor()

    cursor.execute('set search_path to bikesharing,public')
    cursor.execute("select nama from stasiun")
    hasil1 = cursor.fetchall()
    response['stasiun'] = hasil1

    cursor.execute("select penugasan.ktp, person.nama from penugasan, person where person.ktp = penugasan.ktp;")
    pet = cursor.fetchall()
    response['petugas'] = pet

    return render(request, 'reate.html', response)


def post_penugasan(request):
    if(request.method == 'POST'):
        petugas = request.POST['petugas']
        mulai = request.POST['mulai']
        selesai = request.POST['selesai']
        stasiun = request.POST['stasiun']
        a = petugas.split('-')
        ktp = "-".join(a[:3])
        cursor = connection.cursor()
        cursor.execute('set search_path to bikesharing,public')
        cursor.execute(
            "select id_stasiun from stasiun where nama ='"+stasiun+"';")
        id_stasiun = cursor.fetchone()
        cursor.execute("INSERT INTO penugasan(ktp,start_datetime,id_stasiun,end_datetime) values(" +
                       "'"+str(ktp)+"','" + mulai+"','" + str(id_stasiun[0]) + "','" + selesai + "');")

        messages.error(request, "success")
        return HttpResponseRedirect('/penugasan/')
    else:
        messages.error(request, "gagal")
        return HttpResponseRedirect('/penugasan/create')


@csrf_exempt
def delete_penugasan(request):
    if(request.method == "POST"):
        petugas = request.POST['ktp']
        a = petugas.split('-')
        no_ktp = "-".join(a[:3])
        cursor = connection.cursor()
        cursor.execute('set search_path to bikesharing,public')
        cursor.execute("delete from penugasan where ktp='"+no_ktp+"';")
        messages.error(request, "success")
        return HttpResponseRedirect('/penugasan/')
    else:
        messages.error(request, "gagal")
        return HttpResponseRedirect('/penugasan/create')


@csrf_exempt
def update_penugasan(request):
    if(request.method == "POST"):
        petugas = request.POST['petugas']
        a = petugas.split('-')
        no_ktp_baru = "-".join(a[:3])
        mulai = request.POST['mulai']
        selesai = request.POST['selesai']
        stasiun = request.POST['stasiun']
        ktp_lama = request.POST['ktp']

        cursor = connection.cursor()
        cursor.execute('set search_path to bikesharing,public')

        cursor.execute(
            "select start_datetime from penugasan where ktp='"+ktp_lama+"';")
        date_lama = cursor.fetchone()
        date_lama = date_lama[0]

        cursor.execute(
            "select id_stasiun from penugasan where ktp ='"+ktp_lama+"';")
        stasiun_lama = cursor.fetchone()
        stasiun_lama = stasiun_lama[0]

        cursor.execute(
            "select id_stasiun from stasiun where nama='"+stasiun+"';")
        id_stasiun_baru = cursor.fetchone()
        id_stasiun_baru = id_stasiun_baru[0]
        cursor.execute("update penugasan set ktp = '"+no_ktp_baru+"',start_datetime = '"+str(mulai)+"', id_stasiun = '"+id_stasiun_baru +
                       "', end_datetime = '"+str(selesai)+"' where ktp ='"+ktp_lama+"'and start_datetime = '"+str(date_lama)+"'and id_stasiun = '"+stasiun_lama+"';")

        messages.error(request, "success")
        return HttpResponseRedirect('/penugasan/daftarPenugasan')
    else:
        messages.error(request, "gagal")
        return HttpResponseRedirect('/penugasan/daftarPenugasan')


@csrf_exempt
def detailPenugasan(request):
    if(request.method == "POST"):
        response = {}

        ktp = request.POST['ktp']
        cursor = connection.cursor()
        cursor.execute('set search_path to bikesharing,public')
        cursor.execute("select * from penugasan where ktp='"+ktp+"';")
        penugasan = cursor.fetchall()
        cursor.execute("select nama from person where ktp='"+ktp+"';")
        nama = cursor.fetchone()
        nama_stasiun = cursor.fetchall()
        cursor.execute("select s.nama from stasiun as s, penugasan as p where p.ktp='" +
                       ktp+"' and p.id_stasiun=s.id_stasiun")
        stasiun = cursor.fetchone()
        cursor.execute("select nama from stasiun")
        list_stasiun = cursor.fetchall()
        cursor.execute(
            "select penugasan.ktp, person.nama from penugasan, person where person.ktp = penugasan.ktp;")
        petugas = cursor.fetchall()
        response['petugas'] = petugas
        response['penugasan'] = penugasan[0]
        response['nama'] = nama[0]
        response['stasiun'] = stasiun[0]
        response['list_stasiun'] = list_stasiun
        messages.error(request, "success")
        return JsonResponse(response)

    else:
        messages.error(request, "gagal")
        return HttpResponseRedirect('/penugasan//')
