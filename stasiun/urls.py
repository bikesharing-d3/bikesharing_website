from django.urls import path
from . import views

urlpatterns = [
    path('createStasiun/', views.create_stasiun, name='create_stasiun'),
    path('daftarStasiun/', views.daftarStasiun, name='daftarStasiun'),
    path('post-stasiun/',views.postStasiun, name = 'postStasiun'),
    path('delete-stasiun/',views.deleteStasiun, name = 'deleteStasiun'),
    path('update-stasiun/',views.updateStasiun, name = 'updateStasiun'),
    path('detail-update-stasiun/',views.detailUpdateStasiun, name = 'detailUpdateStasiun'),
]
