function deleteList(x) {
    $(".cd-popup").addClass("is-visible")
    $(".cd-popup").attr('id', x)
}

function updateAcara() {
    length = $(".stasiun").attr('id')
    id_acara = $(".modal-body").attr('id')
    judul = $('#judul').val()
    if (judul == '') {
        judul = $('#judul').attr('placeholder')
    }
    deskripsi = $('#deskripsi').val()
    if (deskripsi == '') {
        deskripsi = $('#deskripsi').attr('placeholder')
    }
    biaya = $('#biaya').val()
    tgl_mulai = $('#tanggalMulai').val()
    if (tgl_mulai == '') {
        tgl_mulai = $('#tanggalMulai').attr('placeholder')
    }
    tgl_selesai = $('#tanggalSelesai').val()
    if (tgl_selesai == '') {
        tgl_selesai = $('#tanggalSelesai').attr('placeholder')
    }

    list_stasiun = []
    for (i = 0; i < length; i++) {
        list_stasiun.push(
            $(`#inputStasiun` + i).val()

        )
    }
    all_data = {
        'id_acara': id_acara,
        'judul': judul,
        'deskripsi': deskripsi,
        'fee': biaya,
        'mulai': tgl_mulai,
        'selesai': tgl_selesai,
        'stasiun': list_stasiun,
    }
    console.log(all_data)


    $.ajax(
        {
            url: "/acara/update-acara/",
            datatype: 'json',
            data: all_data,
            method: 'POST',
            success: function (result) {
                location.reload()
            },
            error: function (a) {
                console.log(a)
                location.reload();

            }

        }
    );

}

function cancelFunction() {
    $('.cd-popup').removeClass('is-visible')
}

function yesFunction() {
    id = $(".cd-popup").attr('id')
    $.ajax(
        {
            url: "/acara/delete-acara/",
            datatype: 'json',
            data: { 'id': id },
            method: 'POST',
            success: function (result) {
                location.reload()
            }
        }
    );
}
function updateModal(x) {
    $(".modal-body").attr('id', x)

    $.ajax(
        {
            url: "/acara/detail-update-acara/",
            datatype: 'json',
            data: { 'id': x },
            method: 'POST',
            success: function (result) {
                console.log(result)
                biaya = 'gratis'
                biaya_opt = '<option>berbayar</option>'
                if (result.acara[3] == false) {
                    biaya = 'berbayar'
                    biaya_opt = '<option>gratis</option>'
                }
                all_stasiun = ''
                for (i = 0; i < result.stasiun.length; i++) {
                    all_stasiun += '<option>' + result.stasiun[i] + '</option>'
                }
                stasiun = ''
                for (j = 0; j < result.nama_stasiun.length; j++) {
                    stasiun += `<br><label for="formGroupExampleInput2">Stasiun</label>
                <select name="stasiun" id="inputStasiun`+ j + `" class="form-control"placeholder="Stasiun"required>
                    <option selected>`+ result.nama_stasiun[j] + `</option>` +
                        all_stasiun + `
                </select>`

                }
                $('#judul').attr('placeholder', result.acara[1])
                $('#deskripsi').attr('placeholder', result.acara[2])
                $('#tanggalMulai').attr('placeholder', result.acara[3])
                $('#tanggalSelesai').attr('placeholder', result.acara[4])
                $('#biaya').html(
                    `<option selected>` + biaya + `</option>` +
                    biaya_opt
                )
                $('#stasiun').html(stasiun)
                $('.stasiun').attr('id', result.nama_stasiun.length)
            }
        }
    );
}