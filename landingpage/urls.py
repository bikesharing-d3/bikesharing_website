from django.urls import path
from . import views
from django.contrib import admin

#url for app
urlpatterns = [
    path('', views.index, name='index_landingpage'),
]