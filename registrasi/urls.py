from django.contrib import admin
from django.urls import path
from . import views

#url for app
urlpatterns = [
    path('user/', views.user, name='user'),
    path('user/login/', views.login, name='login'),
    path('user/post-login/', views.postLogin, name='postLogin'),
    path('user/logout/', views.logout, name='logout'),
    path('user/anggota/', views.anggota, name='anggota'),
    path('user/post-anggota/', views.postAnggota, name='postAnggota'),
    path('user/petugas/', views.petugas, name='petugas'),
    path('user/post-petugas/', views.postPetugas, name='postPetugas'),
    path('user/laporan/', views.daftar_laporan, name='laporan'),
    path('transaksi/', views.transaksi, name='transaksi'),
    path('transaksi/top_up/', views.top_up, name='top_up'),
    path('transaksi/post-topup/', views.postTopup, name='postTopup'),
    path('transaksi/riwayat/', views.riwayat, name='riwayat'),
]