from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.db import connection
from django.contrib import messages
from random import randint

response = {}

def user(request):
    if ('nama' in request.session.keys()):
        return HttpResponseRedirect('/')
    else:
        return render(request, 'user.html', response)

def login(request):
    if ('nama' in request.session.keys()):
        return HttpResponseRedirect('/')
    else:
        return render(request,'login.html')

def logout(request):
    request.session.flush()
    return HttpResponseRedirect('/')

def postLogin(request):
    ktp = str(request.POST['ktp'])
    email = request.POST['email']

    cursor=connection.cursor()
    cursor.execute('set search_path to bikesharing,public')
    cursor.execute("SELECT * from person where ktp='"+ktp+"' and email='"+email+"'")
    result=cursor.fetchone();
    if (result):
        cursor.execute("SELECT * from petugas where ktp='"+ktp+"'")
        result2 = cursor.fetchone();
        if(result2):
            cursor.execute("SELECT nama from person where ktp='"+ktp+"'")
            nama = cursor.fetchone()
            nama = nama[0]
            request.session['nama'] = nama
            request.session['ktp'] = ktp
            request.session['email'] = email
            request.session['role'] = 'petugas'
            return HttpResponseRedirect('/penugasan/')
        else:
            cursor.execute("SELECT * from anggota where ktp='"+ktp+"'")
            result3 = cursor.fetchone()
            if(result3):
                cursor.execute("SELECT nama from person where ktp='"+ktp+"'")
                nama = cursor.fetchone()
                nama = nama[0]
                request.session['nama'] = nama
                request.session['ktp'] = ktp
                request.session['email'] = email
                request.session['role'] = 'anggota'
                return HttpResponseRedirect('/sepeda/')
            else:
                messages.error(request, "email atau ktp salah")
                return HttpResponseRedirect('/signup/user/login')
    else:
        messages.error(request, "email atau ktp salah")
        return HttpResponseRedirect('/signup/user/login')

def anggota(request):
    if ('nama' in request.session.keys()):
        return HttpResponseRedirect('/')   
    else:
        return render(request, 'anggota.html', response)

def postAnggota(request):
    nama = request.POST['nama']
    email = request.POST['email']
    hp = request.POST['hp']
    tanggal = request.POST['tanggal']
    ktp=request.POST['ktp']
    alamat = request.POST['alamat']
    points = '0'
    saldo = '0'
    no_kartu = generate_no_kartu()
    cursor = connection.cursor()
    cursor.execute("set search_path to bikesharing,public")
    cursor.execute("SELECT * from anggota where ktp='"+ktp+"'")
    
    result=cursor.fetchone()
    
    if (result):
        if len(result)>0:
            messages.error(request, "Registration failed! "+ktp+" has already been registered")
            return HttpResponseRedirect('/signup/user/anggota/')

    cursor=connection.cursor()
    cursor.execute('set search_path to bikesharing')
    cursor.execute("SELECT * from person where email='"+email+"'")
    result=cursor.fetchone();
    if (result):
        if len(result)>0:
            messages.error(request, "Registration failed! "+email+" has already been registered")
            return HttpResponseRedirect('/signup/user/anggota/')

    values = "'"+ktp+"','"+email+"','"+nama+"','"+alamat+"','"+tanggal+"','"+hp+"'"
    query = "INSERT INTO person (ktp,email,nama,alamat,tgl_lahir,no_telp) values (" + values + ")"
    cursor.execute(query)

    values = "'"+no_kartu+"',"+ saldo + "," + points +"," + "'"+ktp+"'"
    query = "INSERT INTO anggota (no_kartu,saldo,points,ktp) values (" + values + ")"
    cursor.execute(query)

    messages.success(request, nama + " is successfully registered!")
    return HttpResponseRedirect('/signup/user/login/')
 

def petugas(request):
    if ('nama' in request.session.keys()):
        return HttpResponseRedirect('/')   
    else:
        return render(request, 'petugas.html', response)

def postPetugas(request):
    nama= request.POST['nama']
    email= request.POST['email']
    hp = request.POST['hp']
    tanggal =request.POST['tanggal']
    alamat = request.POST['alamat']
    gaji='30000'
    ktp=request.POST['ktp']

    cursor=connection.cursor()
    cursor.execute('set search_path to bikesharing')
    cursor.execute("SELECT * from petugas where ktp='"+ktp+"'")
    result=cursor.fetchone();
    if (result):
        if len(result)>0:
            messages.error(request, "Registration failed! "+ktp+" has already been registered")
            return HttpResponseRedirect('/signup/user/petugas/')

    cursor=connection.cursor()
    cursor.execute('set search_path to bikesharing')
    cursor.execute("SELECT * from person where email='"+email+"'")
    result=cursor.fetchone();
    if (result):
        if len(result)>0:
            messages.error(request, "Registration failed! "+email+" has already been registered")
            return HttpResponseRedirect('/signup/user/petugas/')



    values = "'"+ktp+"','"+email+"','"+nama+"','"+alamat+"','"+tanggal+"','"+hp+"'"
    query = "INSERT INTO person (ktp,email,nama,alamat,tgl_lahir,no_telp) values (" + values + ")"
    cursor.execute(query)

    values = "'"+ktp+"',"+ gaji
    query = "INSERT INTO petugas (ktp,gaji) values (" + values + ")"
    cursor.execute(query)


    messages.success(request, nama + " is successfully registered!")
    return HttpResponseRedirect('/signup/user/login/')
 

def generate_no_kartu():
    kartu = randint(10000000, 99999999)
    kartu = str(kartu)
    no_kartu=''
    for i in range(0,8):
        no_kartu= no_kartu + kartu[i]

    cursor=connection.cursor()
    cursor.execute('set search_path to bikesharing')
    cursor.execute("SELECT * from anggota where no_kartu='"+no_kartu+"'")
    result=cursor.fetchone();
    if (result):
        if len(result)>0:
            generate_no_kartu()
    else:
        return no_kartu

def daftar_laporan(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to bikesharing,public')
    response={}
    cursor.execute("select n.nama, l.id_laporan,l.no_kartu_anggota, l.datetime_pinjam, l.status, p.denda from person as n, laporan as l,  anggota as a, peminjaman as p where  n.ktp = a.ktp and a.no_kartu = l.no_kartu_anggota and l.no_kartu_anggota = p.no_kartu_anggota and l.datetime_pinjam= p.datetime_pinjam and l.nomor_sepeda = p.nomor_sepeda;")
    result = cursor.fetchall()
    response['laporan'] = result
    print(response)
    return render (request, 'daftar_laporan.html', response)

def transaksi(request):
    return render(request, 'transaksi.html', response)

def top_up(request):
    return render(request, 'top_up.html', response)

def postTopup(request):  
    cursor = connection.cursor()
    cursor.execute('set search_path to bikesharing,public')
    if(request.method=="POST"):
        inputSaldo = request.POST['topup']    
        ktp = request.session['ktp']
        cursor.execute("select saldo from anggota where ktp ='"+ktp+"' ")
        saldo = cursor.fetchall()
        for row in saldo:
            saldo = row[0]
        
        saldoakhir = int(saldo) + int(inputSaldo)
        cursor.execute("UPDATE anggota SET saldo = '"+str(saldoakhir)+"' where ktp ='"+ktp+"' ")
        messages.error(request, "Transaction succeeded!")
        return HttpResponseRedirect('/signup/transaksi/top_up/')
        
    else:
        messages.error(request, "Transaction failed!")
        return HttpResponseRedirect('/signup/transaksi/top_up/')


def riwayat(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to bikesharing,public')
    response={}
    ktp = request.session['ktp']
    cursor.execute("select t.date_time,t.jenis,t.nominal from anggota as a, transaksi as t where a.ktp='"+ktp+"' and t.no_kartu_anggota = a.no_kartu;")
    
    hasil1 = cursor.fetchall()
    response['transaksi'] = hasil1

    return render (request, 'riwayat_transaksi.html',response)


