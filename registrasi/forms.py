from django import forms

class LoginForm(forms.Form):
    no_ktp = forms.CharField(required=True, max_length=30, widget=forms.TextInput(attrs={'label': 'No KTP', 'id' : 'no_ktp'}))
    email = forms.CharField(required=True, max_length=30, widget=forms.TextInput(attrs={'type': 'email', 'placeholder':'example@email.com', 'label': 'Email', 'id' : 'email'}))

class AdminForm(forms.Form):
    no_ktp = forms.CharField(required=True, max_length=30, widget=forms.TextInput(attrs={'label': 'No KTP', 'id' : 'no_ktp'}))
    nama = forms.CharField(required=True, max_length=30, widget=forms.TextInput(attrs={'label': 'Nama', 'id' : 'nama'}))
    email = forms.CharField(required=True, max_length=30, widget=forms.TextInput(attrs={'type': 'email', 'placeholder':'example@email.com', 'label': 'Email', 'id' : 'email'}))
    tgl_lahir = forms.DateField(label='Tanggal lahir', required=False, widget=forms.DateInput(attrs={'type' : 'tgl_lahir'}))
    no_telp = forms.CharField(required=True, max_length=30, widget=forms.TextInput(attrs={'label': 'Nomor telepon', 'id' : 'no_telp'}))
    alamat = forms.CharField(required=True, max_length=30, widget=forms.TextInput(attrs={'label': 'Alamat', 'id' : 'alamat'}))

class PetugasForm(forms.Form):
    no_ktp = forms.CharField(required=True, max_length=30, widget=forms.TextInput(attrs={'label': 'No KTP', 'id' : 'no_ktp'}))
    nama = forms.CharField(required=True, max_length=30, widget=forms.TextInput(attrs={'label': 'Nama', 'id' : 'nama'}))
    email = forms.CharField(required=True, max_length=30, widget=forms.TextInput(attrs={'type': 'email', 'placeholder':'example@email.com', 'label': 'Email', 'id' : 'email'}))
    tgl_lahir = forms.DateField(label='Tanggal lahir', required=False, widget=forms.DateInput(attrs={'type' : 'tgl_lahir'}))
    no_telp = forms.CharField(required=True, max_length=30, widget=forms.TextInput(attrs={'label': 'Nomor telepon', 'id' : 'no_telp'}))
    alamat = forms.CharField(required=True, max_length=30, widget=forms.TextInput(attrs={'label': 'Alamat', 'id' : 'alamat'}))

class AnggotaForm(forms.Form):
    no_ktp = forms.CharField(required=True, max_length=30, widget=forms.TextInput(attrs={'label': 'No KTP', 'id' : 'no_ktp'}))
    nama = forms.CharField(required=True, max_length=30, widget=forms.TextInput(attrs={'label': 'Nama', 'id' : 'nama'}))
    email = forms.CharField(required=True, max_length=30, widget=forms.TextInput(attrs={'type': 'email', 'placeholder':'example@email.com', 'label': 'Email', 'id' : 'email'}))
    tgl_lahir = forms.DateField(label='Tanggal lahir', required=False, widget=forms.DateInput(attrs={'type' : 'tgl_lahir'}))
    no_telp = forms.CharField(required=True, max_length=30, widget=forms.TextInput(attrs={'label': 'Nomor telepon', 'id' : 'no_telp'}))
    alamat = forms.CharField(required=True, max_length=30, widget=forms.TextInput(attrs={'label': 'Alamat', 'id' : 'alamat'}))

class TransaksiForm(forms.Form):
    top_up = forms.CharField(required=True, max_length=30, widget=forms.TextInput(attrs={'label': 'Nominal', 'id' : 'top_up'}))