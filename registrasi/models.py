from django.db import models

# Create your models here.
class Login(models.Model):
    no_ktp = models.CharField(max_length=30)
    email = models.EmailField(max_length=30, primary_key=True, unique=True)

class Admin(models.Model):
    no_ktp = models.CharField(max_length=30)
    nama = models.CharField(max_length=30)
    email = models.EmailField(max_length=30, primary_key=True, unique=True)
    tgl_lahir = models.DateField()
    no_telp = models.CharField(max_length=30)
    alamat = models.CharField(max_length=30)

class Petugas(models.Model):
    no_ktp = models.CharField(max_length=30)
    nama = models.CharField(max_length=30)
    email = models.EmailField(max_length=30, primary_key=True, unique=True)
    tgl_lahir = models.DateField()
    no_telp = models.CharField(max_length=30)
    alamat = models.CharField(max_length=30)

class Anggota(models.Model):
    no_ktp = models.CharField(max_length=30)
    nama = models.CharField(max_length=30)
    email = models.EmailField(max_length=30, primary_key=True, unique=True)
    tgl_lahir = models.DateField()
    no_telp = models.CharField(max_length=30)
    alamat = models.CharField(max_length=30)

class Transaksi(models.Model):
    top_up = models.CharField(max_length=30)
