"""bikesharing URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import url, include
from django.urls import path
# from landingpage.views import *
from voucher import views
from penugasan import views
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('landingpage.urls')),
    path('voucher/', include('voucher.urls')),
    path('acara/', include('acara.urls')),
    path('penugasan/', include('penugasan.urls')),
    path('signup/', include('registrasi.urls')),
    path('stasiun/', include('stasiun.urls')),
    path('sepeda/', include('sepeda.urls')),
]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
